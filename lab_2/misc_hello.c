#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>

MODULE_DESCRIPTION("Misc Hello World");
MODULE_LICENSE("GPL");

static int __init my_init(void)
{
	int retval = 0;
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s(%d)\n", __func__, retval);
	return retval;
}

static void __exit my_exit(void)
{
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);
}

module_init(my_init);
module_exit(my_exit);
