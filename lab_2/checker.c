#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdarg.h>

#define BUFFER_SIZE (1024)

static const char *color_array[] = {
	[0] = "\033[91m",
	[1] = "\033[32m",
	[2] = "\033[33m",
	[3] = "\033[0m",
};

typedef enum {
	RED,
	GREEN,
	YELLOW,
	RESET,
} color_t;

int pr_msg(color_t color, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	printf("%s", color_array[color]);
	vprintf(fmt, args);
	printf("%s", color_array[RESET]);
	va_end(args);
}

int main(int argc, char *argv[])
{
	if (argc < 3) {
		printf("Usage:\n");
		printf("%s DEVICE_PATH EXPECT_STRING\n", argv[0]);
		goto out;
	}

	const char *const device_path = argv[1];
	const char *const expect = argv[2];

	int result = access(device_path, F_OK);
	if (result) {
		pr_msg(RED, "FAIL\n");
		pr_msg(YELLOW, "Path: `%s` is not exist.\n", device_path);
		goto out;
	}

	int fp = open(device_path, O_RDWR);
	if (fp == 0) {
		pr_msg(RED, "FAIL\n");
		pr_msg(YELLOW, "Open `%s` fail.\n", device_path);
		goto out;
	}

	char rbuf[BUFFER_SIZE];
	memset(rbuf, 0, BUFFER_SIZE);
	size_t n = read(fp, rbuf, BUFFER_SIZE);

	if (errno) {
		pr_msg(RED, "FAIL\n");
		pr_msg(YELLOW, "read `%s` fail.\n", device_path);
		goto close_file;
	}

	close(fp);

	char *actually = strtok(rbuf, "\n");
	int is_different = strncmp(rbuf, expect, BUFFER_SIZE);

	if (is_different) {
		pr_msg(RED, "FAIL\n");
		pr_msg(YELLOW, "GOT: %s\n", actually);
		pr_msg(YELLOW, "WANT: %s\n", expect);
		exit(EXIT_FAILURE);
	} else {
		pr_msg(GREEN, "PASS\n");
		exit(EXIT_SUCCESS);
	}

close_file:
	close(fp);
out:
	exit(EXIT_FAILURE);
}

