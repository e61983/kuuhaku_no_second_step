# Lab_2 - 核心空間的 Hello World - 以 MISC 裝置為例

## Description

瞭解如何建立一個 MISC 裝置，並提供與使用者互的介面。

- 建立 misc 裝置 - hello
- 讀取時輸出 `hello`

## Software Requestments

### 1.1 裝置名稱

#### 需求編號(Requirments ID)
- 1.1

#### 功能名稱(Function Name)
- 裝置名稱

#### 刺激(Stimuli)
- 在 /dev 目錄中執行 `ls`

#### 反應(Response)
- ls 的輸出需看到 hello

#### 需求(Requirments)
- /dev/hello 需為裝置節點

#### 宣告(Declarations)
- 於 `Makefile` 中宣告 `TARGET` 為裝置名稱 `hello`

#### 效能(Performance)
- N/A

-------------------------

### 1.2 讀取值

#### 需求編號(Requirments ID)
- 1.2

#### 功能名稱(Function Name)
- 讀取值

#### 刺激(Stimuli)
- 讀取 /dev/hello

#### 反應(Response)
- 得到 `hello world`

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

