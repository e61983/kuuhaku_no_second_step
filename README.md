# Linux Device Driver Development - Second Step

Learn from errors; grow from failures.

## Author

- Yuan <[e61983@gmail.com](mailto:e61983@gmail.com)>, <[huayuan_li@avision.com.tw](mailto:huayuan_li@avision.com.tw)>

## License

<div>
  <img src="https://www.gnu.org/graphics/gplv3-88x31.png">
</div>

## Environment

- 目前只有在 Ubuntu 18.04 進行驗證過，可正確執行。

## Introuction

這個專案配合提供一個自動化的測試環境，讓我們可以依照每個實驗設計進行
Device Driver 開發的練習。

目前我們一共設計了下列實驗:

- Lab_0 - 開發環境建置
- Lab_1 - 使用者空間的 Hello World
- Lab_2 - 核心空間的 Hello World - 以 MISC 裝置為例
- Lab_3 - 字元裝置 (Character Device) 與Sysfs
- Lab_4 - 驅動模型 (Driver Model) 的實現
- Lab_5 - 平台裝置 (Platform Device) 的實作

## How to Start ?

在第一次執行本專案的時候，請先在終端機中執行 `./basic_setup.sh`.  
接著就可以進入各個實驗目錄完成各項練習。  
我們可以在本專案的根目錄下執行 `make` 來快速的觀察目前所有實驗的驗證結果。  
或是進入各個實驗目錄中執行 `make`。

01 - 觀察目前所有實驗的驗證結果。

```sh
make
```

02 - 觀察目前特定實驗的驗證結果。

```sh
make lab_2
```

03 - 進入各個實驗目錄進行驗證。

```sh
cd lab_2
make
```
