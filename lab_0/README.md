# Lab_0 - 開發環境建置

## Description

瞭解如何使用 `apt install`，並知道 Kernel Module 開發時所需要的相關套件。

## Software Requestments

### 1.1 安裝相依套件

#### 需求編號(Requirments ID)
- 1.1

#### 功能名稱(Function Name)

- 安裝下列清單中的套件:
    - build-essential
    - linux-headers-$(uname -r)
    - libncurses5-dev
    - libelf-dev
    - bc
    - cpio
    - lib32z1
    - libssl-dev

#### 刺激(Stimuli)
- 在終端機中輸入 `apt list --intalled`

#### 反應(Response)
- 在 `apt list --intalled` 的輸出要可以看到。

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------
