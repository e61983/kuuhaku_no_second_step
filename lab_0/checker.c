#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define BUFFER_SIZE (1024)

#define CHECK_AND_SHOW_MESSAGE(package)                                              \
	do {                                                                         \
		result = check_package(#package);                                    \
		if (result) {                                                        \
			printf("%sFAIL\n", red);                                     \
			printf("%sPlease use following command to install " #package \
			       "\n",                                                 \
			       yellow);                                              \
			printf("Command : `sudo apt install " package "`\n");        \
			printf("%s", reset);                                         \
			retval |= 1;                                                 \
		}                                                                    \
	} while (0)

static const char *red = "\033[91m";
static const char *green = "\033[32m";
static const char *yellow = "\033[33m";
static const char *reset = "\033[0m";

static int check_package(char *name)
{
	int retval = 0;
	static char command[BUFFER_SIZE];
	static const char *const inquiry_string =
		"sudo apt list --installed 2>/dev/null | grep %s 2>&1 >/dev/null";

	int n = snprintf(command, BUFFER_SIZE, inquiry_string, name);
	if (!n) {
		printf("%sFAIL\n", red);
		printf("Generate quiry string fail\n");
		printf("%s", reset);
		return EXIT_FAILURE;
	}
	return system(command);
}

int main(int argc, char *argv[])
{
	int retval = EXIT_SUCCESS;
	int result = 0; /**< used in `CHECK_AND_SHOW_MESSAGE` marco */

	CHECK_AND_SHOW_MESSAGE("build-essential");
	CHECK_AND_SHOW_MESSAGE("linux-headers-$(uname -r)");
	CHECK_AND_SHOW_MESSAGE("libncurses5-dev");
	CHECK_AND_SHOW_MESSAGE("libelf-dev");
	CHECK_AND_SHOW_MESSAGE("bc");
	CHECK_AND_SHOW_MESSAGE("cpio");
	CHECK_AND_SHOW_MESSAGE("lib32z1");
	CHECK_AND_SHOW_MESSAGE("libssl-dev");

	if (!retval)
		printf("%sPASS%s\n", green, reset);

	exit(retval);
}

