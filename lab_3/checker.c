#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdbool.h>

#include "upcase.h"

#define BUFFER_SIZE (1024)

#define ARRAY_SIZE(a) sizeof(a) / sizeof(((typeof(a)){})[0])

static const char *color_array[] = {
	[0] = "\033[91m",
	[1] = "\033[32m",
	[2] = "\033[33m",
	[3] = "\033[0m",
};

typedef enum {
	RED,
	GREEN,
	YELLOW,
	RESET,
} color_t;

int pr_msg(color_t color, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	printf("%s", color_array[color]);
	vprintf(fmt, args);
	ssize_t n = printf("%s", color_array[RESET]);
	va_end(args);
	return n;
}

static const struct test_case {
	char *input;
	char *expect;
	bool enable;
	bool skip_input;
} test_case[] = {
	/* enable = true*/
	{
		.input = "abcde",
		.expect = "ABCDE",
		.enable = true,
	},
	{
		.input = "abc!@#$%^&*()_)de",
		.expect = "ABC!@#$%^&*()_)DE",
		.enable = true,
	},
	{
		.input = "j#pniio#ayw|h/!gx#m!",
		.expect = "J#PNIIO#AYW|H/!GX#M!",
		.enable = true,
	},
	{
		.input =
			"3Q)S7y8Bp2hN&#6tC*o%TD4CLWlbCnZ2#xmv6VMzE*XbXpwA_p$ifOXU)(xK20HXONJTej<Y(gb+RM78%e8F!*A+kj%4QV^_XzaM",
		.expect =
			"3Q)S7Y8BP2HN&#6TC*O%TD4CLWLBCNZ2#XMV6VMZE*XBXPWA_P$IFOXU)(XK20HXONJTEJ<Y(GB+RM78%E8F!*A+KJ%4QV^_XZAM",
		.enable = true,
	},
	{
		.input = "nxm!edwg+fq$z@*j_(rwcb&*fcdx&)",
		.expect = "NXM!EDWG+FQ$Z@*J_(RWCB&*FCDX&)",
		.enable = true,
	},
	{
		.input = "l)t#g(g!ov",
		.expect = "L)T#G(G!OV",
		.enable = true,
	},
	{
		.input = "pc)b$",
		.expect = "PC)B$",
		.enable = true,
	},

	/* enable = false*/
	{
		.input = "dm+6>QJODN",
		.expect = "dm+6>QJODN",
		.enable = false,
	},
	{
		.input =
			"sANYhu)jV$V@@ITD/bsS>eY#h)9l>BXRJjU+/HK9^bfc6#uxMLS<OItcVk*DwRun*/je!OHPt22$l!bC%nPAw/@G_VS515z+F3Eq",
		.expect =
			"sANYhu)jV$V@@ITD/bsS>eY#h)9l>BXRJjU+/HK9^bfc6#uxMLS<OItcVk*DwRun*/je!OHPt22$l!bC%nPAw/@G_VS515z+F3Eq",
		.enable = false,
	},
	{
		.input = "o%@jNhBu5TyM5qyid7@UZU9wX7_Ka",
		.expect = "o%@jNhBu5TyM5qyid7@UZU9wX7_Ka",
		.enable = false,
	},
	{
		.input = "^@LdlYS51>",
		.expect = "^@LdlYS51>",
		.enable = false,
	},
	{
		.input = "+waW0g%gpfxRZ)H0gFQm1O$k",
		.expect = "+waW0g%gpfxRZ)H0gFQm1O$k",
		.enable = false,
	},
	/* skip_input = true */
	{
		.expect = "+waW0g%gpfxRZ)H0gFQm1O$k",
		.skip_input = true,
	},
	/* input = "" */
	{
		.input = "",
		.expect = "",
		.enable = true,
	},
};

int main(int argc, char *argv[])
{
	if (argc < 2) {
		printf("Usage:\n");
		printf("%s DEVICE_PATH\n", argv[0]);
		goto out;
	}

	const char *const device_path = argv[1];

	int result = access(device_path, F_OK);
	if (result) {
		pr_msg(RED, "FAIL\n");
		pr_msg(YELLOW, "Path: `%s` is not exist.\n", device_path);
		goto out;
	}

	int fp = open(device_path, O_RDWR);
	if (fp == 0) {
		pr_msg(RED, "FAIL\n");
		pr_msg(YELLOW, "Open `%s` fail.\n", device_path);
		goto out;
	}

	char buf[BUFFER_SIZE];

	char *actually = NULL;
	bool is_different = false;
	const char *expect = NULL;

	for (int i = 0; i < ARRAY_SIZE(test_case); i++) {
		if (test_case[i].input) {
			strncpy(buf, test_case[i].input, BUFFER_SIZE);
		}

		if (ioctl(fp, AVI_UPCASE_ENABLE, test_case[i].enable)) {
			pr_msg(RED, "FAIL\n");
			pr_msg(YELLOW, "ioctl fail (enable = %d).\n",
			       test_case[i].enable);
			goto error;
		}

		if (!test_case[i].skip_input) {
			write(fp, buf, strnlen(buf, BUFFER_SIZE));
		}

		memset(buf, 0, BUFFER_SIZE);
		read(fp, buf, BUFFER_SIZE);

		if (errno) {
			pr_msg(RED, "FAIL\n");
			pr_msg(YELLOW, "read `%s` fail.\n", device_path);
			goto error;
		}

		expect = test_case[i].expect;
		is_different = strncmp(buf, expect, BUFFER_SIZE);
		actually = strtok(buf, "\n");

		if (is_different) {
			pr_msg(RED, "(%d) - FAIL\n", i);
			pr_msg(YELLOW, "GOT: %s\n", actually);
			pr_msg(YELLOW, "WANT: %s\n", expect);
			goto error;
		}
	}

	pr_msg(GREEN, "PASS\n");

	close(fp);
	exit(EXIT_SUCCESS);

error:
	close(fp);
	exit(EXIT_FAILURE);
out:
	exit(EXIT_FAILURE);
}

