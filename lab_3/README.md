# Lab_3 - 字元裝置 (Character Device) 與Sysfs

## Description

瞭解如何建立字 Character 裝置，並實作另一種與使用者互動的介面。

- 建立 Character 裝置 - upcase
- 可設定是否要開啟`轉大寫功能`
- 讀取時可以得到上次寫入的資料
- (Optional) 建立 version 裝置屬性

## Software Requestments

### 1.1 裝置名稱

#### 需求編號(Requirments ID)
- 1.1

#### 功能名稱(Function Name)
- 裝置名稱

#### 刺激(Stimuli)
- 在 /dev 目錄中執行 `ls`

#### 反應(Response)
- ls 的輸出需看到 upcase

#### 需求(Requirments)
- /dev/upcase 需為裝置節點

#### 宣告(Declarations)
- 於 `Makefile` 中宣告 `TARGET` 為裝置名稱 `upcase`

#### 效能(Performance)
- N/A

-------------------------

### 1.2 寫入值

#### 需求編號(Requirments ID)
- 1.2

#### 功能名稱(Function Name)
- 寫入值

#### 刺激(Stimuli)
- 開啟 /dev/upcase 檔案後，寫入任意英、數、符號

#### 反應(Response)
- 若裝置處在致能模式下，將欲寫入的字串之英文字母全改為大寫
- 若裝置處在除能模式下，則直接保存原輸入字串

#### 需求(Requirments)
- 在每次讀寫入時，清除上次寫入的字串，反之保留先前寫入的資料

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 1.3 讀取值

#### 需求編號(Requirments ID)
- 1.3

#### 功能名稱(Function Name)
- 讀取值

#### 刺激(Stimuli)
- 開啟 /dev/upcase 檔案後，進行讀取

#### 反應(Response)
- 若寫入時裝置處在致能模式下，需得到英文字母全為大小的前次輸入之字串
- 若寫入時裝置處在除能模式下，需得到前次輸入之字串
- 若寫入時為空字串，則得到空字串

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 1.4 輸入值轉大寫功能致能與除能

#### 需求編號(Requirments ID)
- 1.4

#### 功能名稱(Function Name)
-  輸入值轉大寫功能致能與除能

#### 刺激(Stimuli)
- 開啟 /dev/upcase 檔案後，透過 ioctl 系統呼叫致能或除能 /dev/upcase
輸入值轉大寫功能

#### 反應(Response)
- 修改裝置的輸入值轉大寫功能狀態為使用者的設定狀態(致能或除能)

#### 需求(Requirments)
- 輸入值轉大寫功能需預設為致能

#### 宣告(Declarations)
- 裝置的 ioctl 參數需宣告在 upcase.h 中
- 輸入值轉大寫功能致能與除能需宣告為`AVI_UPCASE_ENABLE`

#### 效能(Performance)
- N/A

