LABS = lab_0 lab_1 lab_2 lab_3 lab_4 lab_5
CLEAN_LABS = $(addsuffix .clean, $(LABS))

all: clean $(LABS)

.PHONY: $(LABS)
$(LABS):
	@echo -n "Checking...$@ : "
	@$(MAKE) -s -C $@ check

.PHONY: clean
clean: $(CLEAN_LABS)

%.clean:
	- $(MAKE) -s -C $* clean
