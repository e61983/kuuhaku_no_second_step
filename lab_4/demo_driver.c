#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_DESCRIPTION("Device Model - Driver");
MODULE_LICENSE("GPL");

static int __init demo_driver_init(void)
{
	int retval = 0;
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);
	return retval;
}

static void __exit demo_driver_exit(void)
{
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);
}

module_init(demo_driver_init);
module_exit(demo_driver_exit);
