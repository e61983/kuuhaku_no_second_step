#pragma once
#include <linux/device.h>
#include "demo_device.h"

struct upcase_operations {
	size_t (*process)(const char *input, char *output, size_t len);
};

struct upcase_dev {
	unsigned int major;
	struct class *class;
	struct device *device;
    struct upcase_operations *ops;
};

struct demo_driver {
	struct device_driver driver;
	int (*probe)(struct demo_device *dev);
	int (*remove)(struct demo_device *dev);
};

static inline struct demo_driver *dri_to_demo_driver(struct device_driver *dri)
{
	return container_of(dri, struct demo_driver, driver);
}

static inline struct upcase_dev *dev_to_upcase_dev(struct device *dev)
{
	struct demo_device *_dev = dev_to_demo_device(dev);
	return dev_get_drvdata(&_dev->dev);
}

