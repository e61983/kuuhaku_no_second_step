#pragma once

#include <linux/device.h>

#define NAME_SIZE (16UL)

struct demo_device {
	char name[NAME_SIZE];
	unsigned int id;
	struct device dev;
};

static inline struct demo_device *dev_to_demo_device(struct device *dev)
{
	return container_of(dev, struct demo_device, dev);
}
