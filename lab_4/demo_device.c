#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

#include "demo_bus.h"

MODULE_DESCRIPTION("Device Model - Device");
MODULE_LICENSE("GPL");

static int __init demo_device_init(void)
{
	int retval = 0;
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);
	return retval;
}

static void __exit demo_device_exit(void)
{
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);
}

module_init(demo_device_init);
module_exit(demo_device_exit);
