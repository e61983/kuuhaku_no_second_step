#pragma once

#include "demo_device.h"
#include "demo_driver.h"

extern struct device demo_bus;
extern struct bus_type demo_bus_type;

extern int demo_device_register(struct demo_device *dev);
extern void demo_device_unregister(struct demo_device *dev);

extern int demo_driver_register(struct demo_driver *dri);
extern void demo_driver_unregister(struct demo_driver *dri);

static inline void demo_set_drvdata(struct demo_device *dev, void *data)
{
	dev_set_drvdata(&dev->dev, data);
}

static inline void *demo_get_drvdata(struct demo_device *dev)
{
	return dev_get_drvdata(&dev->dev);
}
