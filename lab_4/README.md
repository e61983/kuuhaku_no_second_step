# Lab_4 - 驅動模型 (Driver Model) 的實現

## Description

瞭解如何實現 Driver Model，並實作出自己的 Bus、Device 與 Device Driver

- 建立自己的 `Demo Bus`
- 建立自己的 `Demo Device`
- 建立自己的 `Demo Device Driver`
- 建立與使用者互動的裝置介面 - upcase
- upcase 可設定是否要開啟`轉大寫功能`
- upcase 讀取時可以得到上次寫入的資料

## Software Requestments

### 1.1 Upcase 裝置名稱

#### 需求編號(Requirments ID)
- 1.1

#### 功能名稱(Function Name)
- Upcase 裝置名稱

#### 刺激(Stimuli)
- 在 /dev 目錄中執行 `ls`

#### 反應(Response)
- ls 的輸出需看到 upcase

#### 需求(Requirments)
- /dev/upcase 需為裝置節點

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 1.2 寫入值

#### 需求編號(Requirments ID)
- 1.2

#### 功能名稱(Function Name)
- 寫入值

#### 刺激(Stimuli)
- 開啟 /dev/upcase 檔案後，寫入任意英、數、符號

#### 反應(Response)
- 若裝置處在致能模式下，將欲寫入的字串之英文字母全改為大寫
- 若裝置處在除能模式下，則直接保存原輸入字串

#### 需求(Requirments)
- 在每次讀寫入時，清除上次寫入的字串，反之保留先前寫入的資料

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 1.3 讀取值

#### 需求編號(Requirments ID)
- 1.3

#### 功能名稱(Function Name)
- 讀取值

#### 刺激(Stimuli)
- 開啟 /dev/upcase 檔案後，進行讀取

#### 反應(Response)
- 若寫入時裝置處在致能模式下，需得到英文字母全為大小的前次輸入之字串
- 若寫入時裝置處在除能模式下，需得到前次輸入之字串
- 若寫入時為空字串，則得到空字串

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 1.4 輸入值轉大寫功能致能與除能

#### 需求編號(Requirments ID)
- 1.4

#### 功能名稱(Function Name)
-  輸入值轉大寫功能致能與除能

#### 刺激(Stimuli)
- 開啟 /dev/upcase 檔案後，透過 ioctl 系統呼叫致能或除能 /dev/upcase
輸入值轉大寫功能

#### 反應(Response)
- 修改裝置的輸入值轉大寫功能狀態為使用者的設定狀態(致能或除能)

#### 需求(Requirments)
- 輸入值轉大寫功能需預設為致能

#### 宣告(Declarations)
- 裝置的 ioctl 參數需宣告在 upcase.h 中
- 輸入值轉大寫功能致能與除能需宣告為`AVI_UPCASE_ENABLE`

#### 效能(Performance)
- N/A

-------------------------

### 2.1 Demo Bus 名稱

#### 需求編號(Requirments ID)
- 2.1

#### 功能名稱(Function Name)
-   Demo Bus 名稱

#### 刺激(Stimuli)
- 在 /sys/bus 目錄中執行 `ls`

#### 反應(Response)
- ls 的輸出需看到 demo_bus

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 2.2 提供 Demo Device 注冊函數與取消注冊函數

#### 需求編號(Requirments ID)
- 2.2

#### 功能名稱(Function Name)
-  提供 Demo Device 注冊函數與取消注冊函數

#### 刺激(Stimuli)
- 在 Kernel space 中提供一函數，使其他程式可以透過此函數注冊 `Demo Device`

#### 反應(Response)
- 將欲注冊之 `Demo Device` 加入 `Demo Bus` 中
- 將 `Demo Device` 結構體內的 `struct device` 之 parent 指向 `Demo Bus` 的 `struct device`
- 將 `Demo Device` 結構體內的 `struct device` 之 bus 指向 `Demo Bus` 的 `struct bus_type`
- 以 `Demo Device` 的名稱與 ID 為 `Demo Device` 命名

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 2.3 提供 Demo Driver 注冊函數與取消注冊函數

#### 需求編號(Requirments ID)
- 2.3

#### 功能名稱(Function Name)
- 提供 `Demo Driver` 注冊函數與取消注冊函數

#### 刺激(Stimuli)
- 在 Kernel space 中提供一函數，使其他程式可以透過此函數注冊 `Demo Driver`

#### 反應(Response)
- 將欲注冊之 `Demo Driver` 加入 `Demo Bus` 中
- 將 `Demo Driver` 結構體內的 `struct device` 之 bus 指向 `Demo Bus` 的 `struct bus_type`

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 2.4 Match 函數

#### 需求編號(Requirments ID)
- 2.4

#### 功能名稱(Function Name)
- Match 函數

#### 刺激(Stimuli)
- 當 Kernel 呼叫此 Bus 的 Match 函數時

#### 反應(Response)
- 以 Demo Device 的名稱與 Driver 的名稱進行比對。

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------

### 3.1 Demo Device 名稱

#### 需求編號(Requirments ID)
- 3.1

#### 功能名稱(Function Name)
-   Demo Device 名稱

#### 刺激(Stimuli)
- 在 /sys/bus/devices 目錄中執行 `ls`

#### 反應(Response)
- ls 的輸出需看到 `demo_upcase-0`

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- 在 `Demo Device` 結構體內需含有`名稱`欄位
    - 型態: char []
    - 變數名稱: name

- 在 `Demo Device` 結構體內需含有 `ID` 欄位
    - 型態: unsigned int
    - 變數名稱: id

- 在 `Demo Device` 結構體內需含有 `struct device` 欄位

#### 效能(Performance)
- N/A

-------------------------

### 4.1 Demo Driver 名稱

#### 需求編號(Requirments ID)
- 4.1

#### 功能名稱(Function Name)
-   Demo Driver 名稱

#### 刺激(Stimuli)
- 在 /sys/bus/drivers 目錄中執行 `ls`

#### 反應(Response)
- ls 的輸出需看到 `demo_upcase`

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- 在 `Demo Driver` 結構體內需含有 `struct device_driver` 欄位

#### 效能(Performance)
- N/A
