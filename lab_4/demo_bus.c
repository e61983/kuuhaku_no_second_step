#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

#include "demo_bus.h"

MODULE_DESCRIPTION("Device Model - Bus");
MODULE_LICENSE("GPL");

static int __init demo_bus_init(void)
{
	int retval = 0;

	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);

	return retval;
}

static void __exit demo_bus_exit(void)
{
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);
}

module_init(demo_bus_init);
module_exit(demo_bus_exit);
