#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

#include <linux/platform_device.h>

MODULE_DESCRIPTION("Upcase - platform device version");
MODULE_LICENSE("GPL");

static int upcase_probe(struct platform_device *pdev)
{
	int retval = 0;
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s(%d)\n", __func__, retval);
	return retval;
}

static int upcase_remove(struct platform_device *pdev)
{
	int retval = 0;
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s(%d)\n", __func__, retval);
	return retval;
}

static struct platform_driver pdri = {
	.probe = upcase_probe,
	.remove = upcase_remove,
	.driver =
		{
			.owner = THIS_MODULE,
			.name = "upcase-core",
		},
};

module_platform_driver(pdri);

