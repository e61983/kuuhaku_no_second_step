#pragma once

#define UPCASE_MAGIC 'A'
#define UPCASE_ENABLE _IOW(UPCASE_MAGIC, 1, int)

#define UPCASE_IOC_MAX_NUMBER    1
