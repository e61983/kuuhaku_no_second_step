#pragma once
#include <linux/device.h>
#include <linux/platform_device.h>

struct upcase_operations {
	size_t (*process)(const char *input, char *output, size_t len);
};
