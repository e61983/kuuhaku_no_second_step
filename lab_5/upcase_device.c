#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

MODULE_AUTHOR("Yuan <huayuan_li@avision.com.tw>");
MODULE_LICENSE("GPL");

static int __init upcase_init(void)
{
	int retval = 0;
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s(%d)\n", __func__, retval);
	return retval;
}

static void __exit upcase_exit(void)
{
	pr_debug("Entry...%s\n", __func__);
	pr_debug("Exit...%s\n", __func__);
}

module_init(upcase_init);
module_exit(upcase_exit);

