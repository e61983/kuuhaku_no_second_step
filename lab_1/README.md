# Lab_1 - 使用者空間的 Hello World

## Description

複習 C Language 與 Makefile 的撰寫。

- 練習撰寫 Makefile
- 撰寫 hello world 程式

## Software Requestments

### 1.1 輸出

#### 需求編號(Requirments ID)
- 1.1

#### 功能名稱(Function Name)
- 輸出

#### 刺激(Stimuli)
- 在終端機中執行程式

#### 反應(Response)
- 程式執行後需輸出 `hello`

#### 需求(Requirments)
- N/A

#### 宣告(Declarations)
- N/A

#### 效能(Performance)
- N/A

-------------------------
