#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define BUFFER_SIZE (1024U)

static const char *red = "\033[91m";
static const char *green = "\033[32m";
static const char *yellow = "\033[33m";
static const char *reset = "\033[0m";

int main(int argc, char *argv[])
{
    if(argc < 3){
        printf("Usage:\n");
        printf("%s TARGET_PROGRAM EXPECT_STRING\n", argv[0]);
        goto out;
    }

    const char * const target_program = argv[1];
    const char * const expect = argv[2];

    FILE *fp = popen(target_program, "r");

    if (fp == NULL){
        errno = ENOENT;
        perror(argv[0]);
        goto out;
    }

    char rbuf[BUFFER_SIZE];
    size_t n = fread(rbuf,sizeof(rbuf), 1, fp);

    if(n == 0 && ferror(fp)){
        perror(argv[0]);
        goto close_file;
    }

    pclose(fp);

    int is_different = strncmp(rbuf,expect, strnlen(expect, BUFFER_SIZE));

    if(is_different){
        char* actually = strtok(rbuf, "\n");
        printf("%sFAIL\n%s", red, reset);
        printf("%s", yellow);
        printf("GOT: %s\n", actually);
        printf("WANT: %s\n", expect);
        printf("%s", reset);
        exit(EXIT_FAILURE);
    }else{
        printf("%sPASS\n%s", green, reset);
        exit(EXIT_SUCCESS);
    }

close_file:
    pclose(fp);
out:
    exit(EXIT_FAILURE);
}

