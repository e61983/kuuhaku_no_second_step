#!/usr/bin/env bash

MAKE=$(which make)
if [ $? -ne 0 ]; then
    sudo apt install -y make
fi

GIT=$(which git)
if [ $? -ne 0 ]; then
    sudo apt install -y git
fi

VIM=$(which vim)
if [ $? -ne 0 ]; then
    sudo apt install -y vim
fi

GCC=$(which gcc)
if [ $? -ne 0 ]; then
    sudo apt install -y gcc
fi
